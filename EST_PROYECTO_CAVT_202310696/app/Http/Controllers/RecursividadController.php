<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//Se crea una clase llamada RecursividadController
class RecursividadController extends Controller
{
    //Se crea la función publica llamada recursividad con las variables Nfin y N respecetivamente
    public function recursividad($Nfin, $N){
        $N += 1;
        if($N<=$Nfin){
            echo $N, '<br>';
            $this->recursividad($Nfin, $N);
        }
    }
    //Se crea la función publica llamada Incrementable en donde estableceremos un inicio y un final para que imprima un valor establecido
    public function Incrementable(){
        $this->recursividad(100, 0);// Como resultado imprime los numeros establecidos en lista
    }
}
