<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// Se crea la clase MemoriaController
class MemoriaController extends Controller
{
    //Creamos primero una funcion publica llamada memoria
    public function Memoria(){
        // Se crea la variable resultado para guardar el mismo
        $resultado = '';
        $resultado.= "Memoria en uso: ". memory_get_usage() . ' ('. ((memory_get_usage() / 1024) / 1024) .') '; // Se utiliza memory_get_usage para conocer el uso de la memoria
        $resultado.= str_repeat("a", 100);  // Str:repeat nos sirve para repetir 100 veces en este caso "a" y saber en cuestion el uso de memoria al ejecutarlo
        $resultado.="Memoria en uso: ". memory_get_usage() . ' ('. ((memory_get_usage() / 1024) / 1024). ') ';
        $resultado.='Memoria limite: ' . ini_get('memory_limit');// Limitamos el uso de memoria con ini_get
        return  view('memoria',['resultado'=>$resultado]); // Usamos return para mostrar resultados
    }
}
?>