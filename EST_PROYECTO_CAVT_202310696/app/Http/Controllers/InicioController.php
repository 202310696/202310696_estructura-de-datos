<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// Se crea una clase llamada Inicio controller 
class InicioController extends Controller
{
    //Se crea una funcion publica que lleva por nombre Inicio en donde se guardara la información
    public function Inicio(){
        $edad = 26;     //Le asignamos el valor de 26 a la variable edad
        $Aedad = & $edad; // Le asignamos la variable Aedad lo que contiene en la memoria la variable edad que es 26
        unset($edad);
        return view("apuntadores",["Aedad"=>$Aedad]); // Imprime el resultado de Aedad
    }
}