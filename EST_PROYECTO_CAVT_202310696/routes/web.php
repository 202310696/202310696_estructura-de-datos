<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InicioController;
use App\Http\Controllers\RecursividadController;
use App\Http\Controllers\MemoriaController;
use App\Http\Controllers\InterfazGraficaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/apuntadores', [InicioController:: class, "Inicio"]);
Route::get('/recursividad', [RecursividadController:: class, 'Incrementable']);
Route::get('/Memoria', [MemoriaController:: class, 'Memoria']);
Route::get('/interfaz', [InterfazGraficaController:: class, 'Inicio']);