<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    
</head>
<body>
    <h1>Hola Mundo</h1>
    <strong><p>Christian Alexis Vázquez Torres</p>
    </strong>
    
    <div class="container">
           <div class="row">
               <div class="col-6">
                 <h4>Columnas 6</h4>
               </div>
               <div class="col-6">
                 <h4>Columnas 6</h4>
               </div>
           </div>
    </div>

    <div class="mb-3">
  <label for="formFile" class="form-label">Default file input example</label>
  <input class="form-control" type="file" id="formFile">
</div>
<div class="mb-3">
  <label for="formFileMultiple" class="form-label">Multiple files input example</label>
  <input class="form-control" type="file" id="formFileMultiple" multiple>
</div>
<div class="mb-3">
  <label for="formFileDisabled" class="form-label">Disabled file input example</label>
  <input class="form-control" type="file" id="formFileDisabled" disabled>
</div>



<div class="mb-4 row">
    <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
    <div class="col-sm-10">
      <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="correo@ejemplo.com">
    </div>
  </div>
  <div class="mb-3 row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" id="inputPassword">
    </div>
  </div>

  <div class="container-fluid">
      <div class="row">
        <div class="col-lg-6">
          <div class="row">
            <div class="col-md-6" style="background-color:lavenderblush">
	      <img src="https://esports.as.com/2021/02/15/league-of-legends/Bard_1438066248_585956_1440x600.jpg" class="rounded" alt="flower" style="width:50%; margin-top: 10px; margin-bottom: 10px">
            </div>

            <img src="https://cinematicos.net/wp-content/uploads/Fecha-de-lanzamiento-personajes-y-trama-de-Shrek-5-lo.jpg" style="width:50%; margin-top: 10px; margin-bottom: 10px">
</body>
</html>